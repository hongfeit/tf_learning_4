# tf_learning_4

This is a project using offical dataset - Fashion-MNIST.
It builds a neural network to identify the picture in the dataset.
The picture from the dataset belongs to 9 different classes.

> 'T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat', 'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot'

## The network have three layers: 

* First layer is used to format the data: change the data format from 28 *28 to a 784 one dimantion array.
* Second layer is a fully connected layer with 128 neurals. The activation function is relu.
* Third layer is a fully connected layer with 10 neurals. The activation function is softmax.